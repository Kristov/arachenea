/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.meltdown.utils.net;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author kristov
 */

/**
 * Client web http pour meltdown.fr
 * @author Christophe "Kristov" RIVIERE
 */
public class WebClient {
    private boolean debug = true;
    private String urlStr;
    private URL url;
    private BufferedReader input;
    private DataOutputStream output;
    public static HashMap<String, String> cookie = new HashMap<String, String>();
    private HttpURLConnection connection; 
    private String userAgent; // User Agent
    private String method; // La méthode à utiliser
    private Map<String, String> clientHeaders;
    private Map<String, List<String>> serverHeaders;
    private String charset; // charset
    private static String referer; 
    private String dataString; // La chaine créee à partir de dataMap
    private HashMap<String, List<String>> dataMap; // Contient les infos à envoyer en POST ou en GET
    private int responseCode;
    private String responseMsg;
    private StringBuffer rawPageContent;
    private StringBuffer trimmedPageContent;
    static private HashMap<String, HashMap<String, HashMap<String, HashMap<String, Object>>>> cookieMap = new HashMap<String, HashMap<String, HashMap<String, HashMap<String, Object>>>>(); // getDomain, Name, value, options (clef/valeur)
    
    
    public WebClient (String url) {
        this.clientHeaders = new HashMap<String, String>();
        this.dataString = new String();
        try {
            this.dataMap = new HashMap<String, List<String>>();
            this.urlStr = url;
            this.url = new URL(url);
            this.method = "GET";
            //this.userAgent = "Meltdown.fr WebClient (" + System.getProperty("os.arch") + " " + System.getProperty("os.name") + " " + System.getProperty("os.version") + ") – JVM" + System.getProperty("java.version");
            this.userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.5; rv:13.0) Gecko/20100101 Firefox/13.0.1 ";
            System.setProperty("http.agent", this.userAgent);
            this.charset = "UTF-8";
            
            this.serverHeaders = new HashMap<String, List<String>>();
            this.rawPageContent = new StringBuffer();
            this.trimmedPageContent = new StringBuffer();
            
            // Default headers
            this.clientHeaders.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            this.clientHeaders.put("Accept-Language", "en-us,en;q=0.5");
            this.clientHeaders.put("Accept-Encoding", "gzip,deflate");
            this.clientHeaders.put("Connection", "keep-alive");
            
        } catch (MalformedURLException malformedURL) {
            malformedURL.printStackTrace();
        } catch (Exception genericException) {
            genericException.printStackTrace();
        }
    }
    
    public WebClient(String url, String method) {
        this(url);
        if (this.method == null || (!this.method.equals("POST") && !this.method.equals("GET"))) {
                this.method = "GET";
        }
    }
    
    /**
     * Permet de récupérer les en-têtes retournés par le serveur distant.
     */
    private void fetchHTTPHeaders() {
        this.serverHeaders = this.connection.getHeaderFields();
        if (this.debug) {
            System.out.println("[Headers] : " + this.serverHeaders);
        }
    }
    
    /**
     * Permet de récupérer le code réponse et le message renvoyés par le serveur distant.
     * @throws Exception 
     */
    private void fetchHTTPResponseCode() throws Exception {
        List<String> valueList = new LinkedList<String>();
        if (this.serverHeaders.containsKey(null)) {
            valueList = this.serverHeaders.get(null);
            // On a donc une liste dans laquelle on doit boucler
            String responseRegex = "HTTP/1\\.1\\s([0-9]{3})\\s([\\w\\s]+)";
            for (String msg : valueList) {
                Pattern responseRegexPattern = Pattern.compile(responseRegex);
                Matcher responseRegexMatcher = responseRegexPattern.matcher(msg);
                if (responseRegexMatcher.find()) {
                    if (responseRegexMatcher.groupCount() != 2) {
                        throw new Exception("The HTTP response header returned by the remote server is malformed : Unable to retrieve the needed information.");
                    } else {
                        this.responseCode = Integer.valueOf(responseRegexMatcher.group(1));
                        this.responseMsg = responseRegexMatcher.group(2);
                    }
                } else {
                    throw new Exception("There seems to be a problem with either the connection to the remote server or the server's behavior. The response header is probably malformed.");
                }
            }
        }
        if (this.debug) {
            System.out.println("[HTTP RCode] : " + this.responseCode);
            System.out.println("[HTTP RMsg] : " + this.responseMsg);
        }
    }
    
    /**
     * Permet de récupérer, afin de l'utiliser, le code réponse HTTP renvoyé par le serveur distant.
     * @return Le code réponse HTTP
     */
    public int getHTTPResponseCode() {
        return this.responseCode;
    }
    
    /**
     * Permet de récupérer, afin de l'utiliser, le message retourné par le serveur distant.
     * @return 
     */
    public String getHTTPResponseMsg() {
        return this.responseMsg;
    }
    
    /**
     * Indique si le statut de la transaction est positionné à OK (code 200).
     * @return 
     */
    public boolean isOK() {
        boolean r = false;
        if (this.responseCode == 200 && this.responseMsg.equals("OK")) {
            r = true;
        }
        return r;
    }
    
    /**
     * Indique si la la transaction a été un succès (code 2xx).
     * @return 
     */
    public boolean isSuccess() {
        boolean r = false;
        if (this.responseCode >= 200 && this.responseCode < 300) {
            r = true;
        }
        return r;
    }
    
    /**
     * Indique si la requête a abouti à une erreur. Aucune distinction n'est faite quand au type d'erreur.
     * @return 
     */
    public boolean isError() {
        boolean r = false;
        if (this.responseCode >= 400 && this.responseCode < 600) {
            r = true;
        }
        return r;
    }
    
    public boolean isRedirect() {
        boolean r = false;
        if (this.responseCode >= 300 && this.responseCode < 400) {
            r = true;
        }
        return r;
    }
    
    /**
     * Retourne le code de l'erreur s'il existe. Dans le cas où le serveur n'aurait pas renvoyé d'erreur, lève une exception.
     * @return le code de l'erreur
     * @throws Exception 
     */
    public int getErrorCode() throws Exception {
        int r = 0;
        if (this.responseCode < 400 && this.responseCode >= 600) {
            throw new Exception("The status of this request is not positionned as erroneous. Unable to return any error code.");
        } else {
            r = this.responseCode;
        }
        return r;
    }
    
    /**
     * Retourne le message associé à l'erreur s'il existe. Dans le cas où le serveur n'aurait pas renvoyé d'erreur, lève une exception.
     * @return le message associé à l'erreur
     * @throws Exception 
     */
    public String getErrorMsg() throws Exception {
        String r = new String();
        if (this.responseCode < 400 && this.responseCode >= 600) {
            throw new Exception("The status of this request is not positionned as erroneous. Unable to return any error code.");
        } else {
            r = this.responseMsg;
        }
        return r;
    }
    
    /**
     * Récupère les valeurs des en-têtes Set-Cookie et les ajoute à la chaîne cookie pour la connexion en cours.
     */
    private void fetchCookie() {
        if (this.serverHeaders.containsKey("Set-Cookie")) {
            for (String value : this.serverHeaders.get("Set-Cookie")) {
                
                String cookieParsingRegex = "^=?([^\\x00-\\x1F\\x7f;\\s,]+)=([^\\x00-\\x1F\\x7f;\\s,]+)(;(\\s?(Domain|Path|Expires)=([^\\x00-\\x1F\\x7f;]+);?)?(\\s?(Domain|Path|Expires)=([^\\x00-\\x1F\\x7f;]+);?)?(\\s?(Domain|Path|Expires)=([^\\x00-\\x1F\\x7f;]+);?)?(\\s(Secure;|HttpOnly;|Secure|HttpOnly))?(\\s(Secure|HttpOnly))?)?$";
                Pattern cprp = Pattern.compile(cookieParsingRegex, Pattern.CASE_INSENSITIVE);
                Matcher cprm = cprp.matcher(value);
                while (cprm.find()) {
                        /*
                         * 1 = name
                         * 2 = value
                         * 5 = key Expires
                         * 6 = value Expires
                         * 8 = key Path
                         * 9 = value Path
                         * 11 = key Domain
                         * 12 = value Domain
                         * 14 = flag Secure
                         * 16 = flag HttpOnly
                         */
                        
                    if (!WebClient.cookieMap.containsKey(this.getDomain())) {
                        WebClient.cookieMap.put(this.getDomain(), new HashMap());
                    }
                    if (cprm.group(1) != null && cprm.group(2) != null) {
                        WebClient.cookieMap.get(this.getDomain()).put(cprm.group(1), new HashMap());
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).put(cprm.group(2), new HashMap());
                    }
                    if (cprm.group(5) != null && cprm.group(6) != null) {
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).get(cprm.group(2)).put(cprm.group(5), cprm.group(6));
                    }
                    if (cprm.group(8) != null && cprm.group(9) != null) {
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).get(cprm.group(2)).put(cprm.group(8), cprm.group(9));
                    }
                    if (cprm.group(11) != null && cprm.group(12) != null) {
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).get(cprm.group(2)).put(cprm.group(11), cprm.group(12));
                    }
                    if (cprm.group(14) != null) {
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).get(cprm.group(2)).put(cprm.group(14), true);
                    }
                    if (cprm.group(14) == null) {
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).get(cprm.group(2)).put("Secure", false);
                    }
                    if (cprm.group(16) != null) {
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).get(cprm.group(2)).put(cprm.group(16), true);
                    }
                    if (cprm.group(16) == null) {
                        WebClient.cookieMap.get(this.getDomain()).get(cprm.group(1)).get(cprm.group(2)).put("HttpOnly", false);
                    }
                    if (this.debug) {
                        System.out.println("[CMap] : " + WebClient.cookieMap);
                    }
                }
            }
                
            String domain = this.getDomain();
            Set cSet = WebClient.cookieMap.get(this.getDomain()).keySet();
            Iterator iter = cSet.iterator();
            for (int ex = 0; iter.hasNext(); ex++) {
                String next = (String) iter.next();
                String next2 = new String();
                        
                        
                Set vSet = WebClient.cookieMap.get(domain).get(next).keySet();
                Iterator iter2 = vSet.iterator();
                if (iter2.hasNext()) {
                    next2 = (String) iter2.next();
                    
                    if (WebClient.cookie.containsKey(domain) && !WebClient.cookie.get(domain).isEmpty()) {
                        String oldValue = WebClient.cookie.get(domain);
                        WebClient.cookie.put(domain, oldValue + next + "=" + next2 + "; ");
                    }
                    else if (!WebClient.cookie.containsKey(domain) || WebClient.cookie.get(domain).isEmpty()) {
                        WebClient.cookie.put(domain, next + "=" + next2 + "; ");
                    }
                }
                    
            }
        }
        if (this.debug) {
            System.out.println("[Cookie] : " + WebClient.cookie);
            System.out.println("[Final CookieMap] : " + WebClient.cookieMap);
        }
    }
    
    /**
     * Renvoie la chaîne cookie pour la connexion en cours.
     * @return 
     */
    public String getCookie() {
        String r = new String();
        if (WebClient.cookie.containsKey(this.getDomain())) {
            r = WebClient.cookie.get(this.getDomain());
        }
        return r;
    }
    
    /**
     * Renvoie le couple clef/valeur pour ne nom de cookie passé en argument.
     * @param cookieName
     * @return 
     */
    public String getCookie(String cookieName) {
        String r = new String();
        if (WebClient.cookie.containsKey(this.getDomain())) {
            String tmp = WebClient.cookie.get(this.getDomain());
            String[] tmpArray = tmp.split(";");
            for (String value : tmpArray) {
                value = value.trim();
                String[] endVal = value.split("=",2);
                if (endVal[0].equals(cookieName)) {
                    r = endVal[1];
                }
            }
        }
        return r;
    }
    
    /**
     * Permet de forcer la valeur du cookie. Attention, cette méthode ECRASE tout valeur précédente de cookie.
     * @param cookieValue - la valeur du cookie (ne pas ajouter ; à la fin)
     */
    public void setCookie(String cookieValue) {
        String domain = this.getDomain();
        WebClient.cookie.put(domain, cookieValue + ";");
    }
    
    /**
     * Permet d'ajouter une valeur à cookie. Attention, la méthode ajoute elle-même le ; à la fin de la valeur.
     * @param cookieValue - la valeur du cookie (ne pas ajouter ; à la fin)
     */
    public void appendCookie(String cookieValue) {
        String domain = this.getDomain();
        String oldVal = WebClient.cookie.get(domain);
        WebClient.cookie.put(domain, oldVal + " " + cookieValue + ";");
    }
    
    /**
     * Permet de récupérer le code source complet de la page en deux exemplaires : avec ET sans les espaces blancs de mise en page.
     */
    private void fetchPageContent() {
        try {
            String str = new String();
            while ((str = this.input.readLine()) != null) {
            this.rawPageContent.append(str);
            this.trimmedPageContent.append(str.trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Renvoie le code source BRUT (avec les espaces blancs de mise en page) de la page.
     * @return 
     */
    public StringBuffer getRawPageContent() {
        StringBuffer r = new StringBuffer();
        if (this.rawPageContent.length() > 0 && this.rawPageContent != null) {
            return this.rawPageContent;
        }
        return r;
    }
    
    /**
     * Renvoie le code source sans les espaces blancs de mise en page.
     * @return 
     */
    public StringBuffer getTrimmedPageContent() {
        StringBuffer r = new StringBuffer();
        if (this.trimmedPageContent.length() > 0 && this.trimmedPageContent != null) {
            return this.trimmedPageContent;
        }
        return r;
    }
    
    /**
     * Initie la connexion effective au serveur distant et récupère les informations de connexion nécessaires (en-têtes) et contenu de la page.
     * @throws Exception - Renvoie une exception dans le cas où le serveur renvoie un message d'erreur (400>=réponse<600)
     */
    public void doConnect() throws Exception {
        try {
            this.connection = (HttpURLConnection) this.url.openConnection(); //Ouvre la connexion
            this.connection.setDoInput(true); //Permet de lire les infos envoyées depuis le serveur
            this.connection.setRequestProperty("User-Agent", this.userAgent);
            this.createDataString(); //On crée 
            if (!this.dataString.isEmpty() && this.dataString != null && !this.dataMap.isEmpty()) { //Si on a des données à envoyer
                this.connection.setDoOutput(true); //Permet d'envoyer des données au serveur
                this.output = new DataOutputStream(this.connection.getOutputStream()); //On ouvre un flux
                this.output.writeBytes(this.dataString); //On écrit les données dans le flux
                this.output.flush(); // On nettoie le flux
                this.output.close(); //On ferme le flux
            }
            this.sendCookie(); // On envoie les cookies enregistrés pour ce domaine
            this.fetchHTTPHeaders(); // On récupère les en-têtes http
            this.fetchHTTPResponseCode(); // On récupère le code réponse et le message associés renvoyés par le serveur
            this.fetchCookie(); // On récupère les cookies que le serveur nous demande de lui renvoyer
            if (!this.isError()) { // Si le code réponse ne correspond pas à une erreur
                this.input = new BufferedReader(new InputStreamReader(this.connection.getInputStream())); //On ouvre un flux pour la lecture du contenu du document
                this.connection.connect(); //On lance la connexion
                this.fetchPageContent(); //On récupère le contenu de la page
            } else { // Si on reçoit un code réponse correspondant à une erreur
                if (this.getErrorCode() >= 400 && this.getErrorCode() < 500) { // Erreur client
                    throw new Exception("There was an error from the client (HTTP " + this.getErrorCode() + " - " + this.getErrorMsg() + ").");
                } else if (this.getErrorCode() >= 500 && this.getErrorCode() < 500) { // Erreur intene au serveur
                    throw new Exception("There was an error internal to the remote server (HTTP " + this.getErrorCode() + " - " + this.getErrorMsg() + ").");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Retourne le nom de domaine pour la connexion courante.
     * @return 
     */
    public String getDomain() {
        String r = new String();
        String regex = "[\\w]+://(([\\w-_]+\\.)?[\\w-_]+(\\.[\\w]{2,5})?)[/\\w\\.]?";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(this.urlStr);
        if (m.find()) {
            r = m.group(1);
        }
        return r;
    }
    
    /**
     * Ferme la connexion en cours. Indispendable pour ne pas gaspiller les ressources.
     */
    public void doDisconnect() {
        this.connection.disconnect();
    }
    
    /**
     * Permet d'ajouter un couple clef/valeur aux données à passer en paramètres à l'URL.
     * @param key
     * @param value
     * @throws Exception - Renvoie une exception dans le cas où le couple soumis est déjà dans la liste.
     */
    public void addData(String key, String value) throws Exception {
        if (!this.dataMap.containsKey(key)) {
            this.dataMap.put(key, new ArrayList<String>());
        }
            if (this.dataMap.get(key).contains(value)) {
                throw new Exception("The value you have submitted (" + value + ") already exists. Unable to add it.");
            }
            this.dataMap.get(key).add(value);
        if (this.debug) {
            System.out.println("[Data] : " + this.dataMap);
        }
    }
    
    /**
     * Permet d'enregistrer des données à passer en paramètre à l'URL (infos contenues dans un formulaire, par exmple).
     * @param data - Une HashMap qui représente les données sous la forme clef/valeur.
     * @return 
     */
    public int addData(HashMap<String, List<String>> data) {
        Set keys = data.keySet();
        Iterator iter = keys.iterator();
        int x = 0;
        while (iter.hasNext()) {
            String nextKey = (String) iter.next();
            if (!this.dataMap.containsKey(nextKey)) {
                this.dataMap.put(nextKey, new ArrayList<String>());
            }
            for (String value : data.get(nextKey)) {
                if (!this.dataMap.get(nextKey).contains(value)) {
                    this.dataMap.get(nextKey).add(value);
                }
            }
            x++;
        }
        if (this.debug) {
            System.out.println("[Data] : " + this.dataMap);
        }
        return x;
    }
    
    /**
     * Permet de retirer un élément de la chaîne des données à passer en paramètres.
     * @param key
     * @param value 
     */
    public void removeData(String key, String value) {
        if (this.dataMap.containsKey(key)) {
            if (this.dataMap.get(key).contains(value)) {
                this.dataMap.get(key).remove(value);
            }
        }
    }
    
    /**
     * Permet la création de la chaîne de données à passer à l'URL en paramètre.
     */
    
    private void createDataString() {
        if (!this.dataMap.isEmpty()) {
            Set keys = this.dataMap.keySet();
            Iterator iter = keys.iterator();
            for (int x = 0; iter.hasNext(); x++) {
                String key = (String) iter.next();
                if (x != 0) {
                    this.dataString += "&";
                }
                String kd = new String();
                for (int y = 0; y < this.dataMap.get(key).size(); y++) {
                    if (y != 0) {
                        kd += "&";
                    }
                    try {
                        //this.dataString += key + "=" + URLEncoder.encode(this.dataMap.get(key).get(y), this.charset);
                        kd += key + "=" + URLEncoder.encode(this.dataMap.get(key).get(y), this.charset);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.dataString += kd;
            }
        }
        if (this.debug) {
            System.out.println("[DataString] : " + this.dataString);
        }
    }
    
    /**
     * Envoie les cookies sur le flux de connexion courant.
     */
    private void sendCookie() {
        if (WebClient.cookie != null && !WebClient.cookie.isEmpty() && WebClient.cookie.containsKey(this.getDomain())) {
            this.connection.setRequestProperty("Cookie", WebClient.cookie.get(this.getDomain()));
        }
    }
}
